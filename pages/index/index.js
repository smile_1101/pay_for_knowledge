//index.js
//获取应用实例
const app = getApp();

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    isNewUser: true,
    realName: null,
    count: 0
  },

  queryEmployeeByOpenId(openId) {
    let that = this;
    wx.request({
      url: app.globalData.serverUrl + '/user/queryEmployeeByOpenId',
      data: {
        openId: openId
      },
      success: function (resp) {
        console.log(resp);
        if (!resp.data.success) {
          // that.setData({
          //   isNewUser: true
          // });

        } else {
          app.globalData.employeeId = resp.data.data.id;

          // wx.switchTab({
          //   url: '../homePage/index',
          // });
        }
      },
      fail: function (error) {
        console.error(error);
      }
    });
  },

  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    let that = this;

    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true,
      });

    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true,
        });

        wx.getStorage({
          key: 'openid&sessionKey',
          success: function (res) {
            console.log(res.data)

            app.globalData.session_key = res.data.session_key;
            app.globalData.openid = res.data.openid;

            // 获取employeeid
            that.queryEmployeeByOpenId(res.data.openid);

            // if (res.data.errcode == 40029) {
            if(res.errMsg != "getStorage:ok") {
              wx.login({
                success: res => {
                  // 发送 res.code 到后台换取 openId, sessionKey, unionId
                  wx.request({
                    url: app.globalData.serverUrl + '/user/get_openId_sessionKey_unionId',
                    data: {
                      code: res.code
                    },
                    success: function (resp) {
                      console.log(resp);
                      if (resp.data.success) {
                        let obj = JSON.parse(resp.data.data);

                        app.globalData.session_key = obj.session_key;
                        app.globalData.openid = obj.openid;

                        wx.setStorage({
                          key: "openid&sessionKey",
                          data: obj
                        });

                        wx.setStorage({
                          key: "session_key",
                          data: obj.opesession_keynid
                        });

                        // that.queryEmployeeByOpenId(app.globalData.openid);
                        wx.switchTab({
                          url: '../homePage/index',
                        });
                      }
                    },
                    fail: function (error) {
                      console.error(error);

                    }
                  });
                }
              });

            }
            else {
              wx.switchTab({
                url: '../homePage/index',
              });
            }
          },
          fail: function (res) {
            console.log(res);
            wx.login({
              success: res => {
                // 发送 res.code 到后台换取 openId, sessionKey, unionId
                wx.request({
                  url: app.globalData.serverUrl + '/user/get_openId_sessionKey_unionId',
                  data: {
                    code: res.code
                  },
                  success: function (resp) {
                    console.log(resp);
                    if (resp.data.success) {
                      let obj = JSON.parse(resp.data.data);

                      app.globalData.session_key = obj.session_key;
                      app.globalData.openid = obj.openid;

                      wx.setStorage({
                        key: "openid&sessionKey",
                        data: obj
                      });

                      wx.setStorage({
                        key: "session_key",
                        data: obj.opesession_keynid
                      });

                      // that.queryEmployeeByOpenId(app.globalData.openid);
                      wx.switchTab({
                        url: '../homePage/index',
                      });
                    }
                  },
                  fail: function (error) {
                    console.error(error);

                  }
                });
              }
            });
          }
        });
      }

    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      });

    }
  },
  
  bindgetuserinfo(e) {
    console.log(e.detail.userInfo)
    if (e.detail.userInfo) {
      //用户按了允许授权按钮
      app.globalData.userInfo = e.detail.userInfo;

      wx.login({
        success: res => {
          // 发送 res.code 到后台换取 openId, sessionKey, unionId
          wx.request({
            url: app.globalData.serverUrl + '/user/get_openId_sessionKey_unionId',
            data: {
              code: res.code
            },
            success: function (resp) {
              console.log(resp);
              if (resp.data.success) {
                let obj = JSON.parse(resp.data.data);

                app.globalData.session_key = obj.session_key;
                app.globalData.openid = obj.openid;

                wx.setStorage({
                  key: "openid",
                  data: obj.openid
                });

                wx.switchTab({
                  url: '../homePage/index',
                });
              }
            },
            fail: function (error) {
              console.error(error);

            }
          });
        }
      });

    }
  }

});