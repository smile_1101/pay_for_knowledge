// pages/register/index.js

const app = getApp();

Page({
  realName(e) {
    this.setData({
      realName: e.detail.value
    });
  },

  getSessionKey() {
    let session_key = app.globalData.session_key;
    if (session_key == null) {
      wx.login({
        success: res => {
          // 发送 res.code 到后台换取 openId, sessionKey, unionId
          wx.request({
            url: app.globalData.serverUrl + '/user/get_openId_sessionKey_unionId',
            data: {
              code: res.code
            },
            success: function (resp) {
              console.log(resp);
              if (resp.data.success) {
                let obj = JSON.parse(resp.data.data);
                session_key = obj.session_key;

                return session_key;
              }
            },
            fail: function (error) {
              console.error(error);
            }
          });
        }
      });
    }

    // 模拟阻塞
    var start = new Date();
    while (new Date() - start < 15000) { // delay 5 sec
      ;
    }

    return null;
  },

  getPhoneNumber(e) {
    console.log(e);

    if (!app.globalData.userInfo) {
      wx.showToast({
        icon: 'none',
        title: '需要，授权获取用户信息!',
      });

      return;
    }

    if (this.data.realName == null || this.data.realName == "") {
      wx.showToast({
        icon: 'none',
        title: '姓名不能为空!',
      });

      return;
    }

    if (e.detail.errMsg == "getPhoneNumber:ok") {
      let encryptedData = e.detail.encryptedData;
      let iv = e.detail.iv;
      let session_key = app.globalData.session_key;
      // let session_key = this.getSessionKey();
      let appId = "wx90c686a8d5f38c1c";
      let name = this.data.realName;

      let that = this;
      wx.showLoading({
        title: '加载中...',
      });
      wx.request({
        method: "POST",
        url: app.globalData.serverUrl + '/user/createWeixEmployee',
        data: {
          openId: app.globalData.openid,
          encryptedData: encryptedData,
          iv: iv,
          session_key: session_key,
          appId: appId,
          name: name,
          nickName: app.globalData.userInfo.nickName,
          avatarUrl: app.globalData.userInfo.avatarUrl,
          gender: app.globalData.userInfo.gender,
          province: app.globalData.userInfo.province,
          city: app.globalData.userInfo.city,
        },
        success: function (resp) {
          wx.hideLoading();
          console.log(resp);

          app.globalData.employeeId = resp.data.data.id;

          if (resp.data.success) {
            wx.switchTab({
              url: '../homePage/index',
            });

          } else {
            wx.showToast({
              icon: 'none',
              title: '绑定失败',
            });
          }
        },
        fail: function (error) {
          wx.hideLoading();
          console.error(error);
        }
      });
    }
  },

  /**
   * 页面的初始数据
   */
  data: {
    realName: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})